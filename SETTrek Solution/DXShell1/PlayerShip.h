/*
*  FILE          : PlayerShip.h
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-May-29
*  DESCRIPTION   :
*    Handles variables and logic required for ship movement and rendering
*/

#pragma once
#include "graphics.h"
#include "Point.h"
#include "Planet.h"
#include "Constants.h"
#include "D2d1helper.h"
#include <ctgmath>
#include <list>

namespace entities
{
	class PlayerShip
	{
		graphics::Sprite* playerSprite;
		Point playerLocation;
		Point targetLocation;
		Point defaultLocation;
		Planet* currPlanet;			//the current planet being scanned, or nullptr
		double angleToTarget;
		bool hasShields;

		//Stats
		int science;
		int energy;

		void CalcTargetAngle();
		void MovePlayer(float distance);

	public:
		bool isMoving;
		bool onPlanet;
		Point lastClick;

		PlayerShip(graphics::Sprite *shipSprite);
		~PlayerShip() { delete playerSprite; }

		void UpdateLocation(double dt);
		void SetTarget(Point newTarget, D2D1_SIZE_U drawableArea);
		Point GetLocation();
		
		void MissileFired();

		int CheckPlanetCollision(std::list<Planet*> planets);
		int CheckEnemyCollision(Point enemyLoc);

		void ExplorePlanet(int choice);

		int GetScience();
		int GetEnergy();

		std::string GetPScience();
		std::string GetPEnergy();

		void Draw();

		void Reset();
	};
}