/*
*  FILE          : EnemyShip.h
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Mar-12
*  DESCRIPTION   :
*    Handles enemy ship sprites, movement, and damge
*/

#pragma once
#include "Graphics.h"
#include "Point.h"
#include "PlayerShip.h"

namespace entities
{
	class EnemyShip
	{
		graphics::Sprite* enemySpriteHealthy;
		graphics::Sprite* enemeySpriteDamaged;
		Point shipLocation;
		Point playerTarget;
		Point defaultLocation;
		double angleToTarget;
		bool damaged;

		void CalcTargetAngle();
		void MoveEnemy(float distance);

	public:
		EnemyShip(graphics::Sprite* shipSpriteHealthy, graphics::Sprite* shipSpriteDamaged);
		~EnemyShip()
		{
			delete enemySpriteHealthy;
			delete enemeySpriteDamaged;
		}

		void UpdateLocation(PlayerShip* player, double dt);

		Point GetLocation();

		void TakeDamage();

		void Draw();
		void Reset();
	};
}