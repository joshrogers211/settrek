/*
*  FILE          : SETTrek.cpp
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-May-29
*  DESCRIPTION   :
*    Base game class. This class is really abstract and could definitely be
*	 used for other projects (with a minor change to RunGame())
*/

#include "SETTrek.h"
#include "direct2d.h"

namespace main
{
	SETTrek::SETTrek(HINSTANCE hInstance) : gameInstance(hInstance), gameWindow(NULL), paused(true), timer(NULL),
		fps(0), msPerFrame(0.0f), dt(SECONDS_PER_FRAME), maxSkipFrames(10), hasStarted(false), showFramerate(false), 
		d2d(NULL)
	{
		gameWindow = NULL;
	}

	SETTrek::~SETTrek()
	{
		Cleanup();
	}

	//Initializes resources the game depends on
	void SETTrek::InitGame()
	{
		timer = new GameTimer();
		gameWindow = new Window(this);
		d3d = new graphics::Direct3D(this);
		d2d = new graphics::Direct2D(this);

		hasStarted = true;
		return;
	}

	//d3d can handle resizing
	void SETTrek::OnResize()
	{
		d3d->OnResize();
	}

	//Cleans up all resources
	void SETTrek::Cleanup()
	{
		if (d2d)
		{
			delete d2d;
		}
		
		if (gameWindow)
		{
			delete gameWindow;
		}

		if (timer)
		{
			delete timer;
		}
	}

	//Main game loop. Dispatches messages and renders frames at a given interval
	int SETTrek::RunGame()
	{
		timer->Reset();

		double accumulatedTime = 0.0f;
		int numberOfLoops = 0;

		run = true;
		MSG msg = { 0 };

		while (run)
		{
			//Message loop so the window doesn't lock up
			while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);

				if (msg.message == WM_QUIT)
				{
					PlaySound(NULL, NULL, NULL);
					run = false;
					break;
				}

				if (msg.message == WM_USER)
				{
					return END_SCREEN_VAL;
				}
			}

			timer->Tick();

			//If the game isn't paused, run the game
			if (!paused)
			{
				accumulatedTime += timer->GetDeltaTime();

				numberOfLoops = 0;

				//Loop until the desired time has passed for 1 frame
				while (accumulatedTime >= dt && numberOfLoops < maxSkipFrames)
				{
					UpdateGameWorld(dt);
					accumulatedTime -= dt;
					numberOfLoops++;
				}

				//Render a frame
				Render(accumulatedTime / dt);
			}
		}

		EndScreen();

		//End game, but still listen for messages so the window doesn't freeze
		while (1)
		{
			while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);

				if (msg.message == WM_QUIT)
				{
					run = false;
					break;
				}

				if (msg.message == WM_USER)
				{
					return END_SCREEN_VAL;
				}
			}
		}

		return (int)msg.wParam;
	}

	//This is old, and could be removed if I made it pure virtual and renamed
	//a method in SETTrekGame
	//
	//Sends the x/y coords of a click to the real game
	void SETTrek::OnClick(WPARAM wParam, int x, int y)
	{
		OnClickParent(x, y);
	}
}