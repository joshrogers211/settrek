/*
*  FILE          : Planet.h
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Apr-02
*  DESCRIPTION   :
*    Holds a planet sprite, as well as location data, science/energy, and whether
*	 the planet has been landed on or not
*/

#pragma once
#include "Graphics.h"
#include "Point.h"
#include <time.h>

namespace entities
{
	class Planet
	{
		graphics::Sprite* planetSprite;

	public:
		int science;
		int energy;
		bool isActive;
		Point planetLocation;

		Planet::Planet(graphics::Sprite* pSprite) : planetSprite(pSprite), isActive(true)
		{
			planetLocation = planetSprite->GetPosition();

			science = rand() % (PLANET_MAX_SCIENCE - PLANET_MIN_SCIENCE) + PLANET_MIN_SCIENCE;
			energy = rand() % (PLANET_MAX_ENERGY - PLANET_MIN_ENERGY) + PLANET_MIN_ENERGY;
		}

		void Planet::Draw()
		{
			planetSprite->SetPosition(planetLocation.x, planetLocation.y);
			planetSprite->Draw();
		}
	};
}