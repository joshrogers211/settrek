/*
*  FILE          : Graphics.cpp
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Feb-11
*  DESCRIPTION   :
*    Handles the loading, drawing and placement of sprites
*/

#include "Graphics.h"
#include "Direct2D.h"
#include "Constants.h"

#include <WTypes.h>
#include <wincodec.h>

namespace graphics {

	//Creates a new sprite
	Sprite::Sprite(Direct2D *d2d, LPCWSTR fileName, float x, float y, Chroma chromaEffect, bool scaleToSquare, unsigned int drawOrder) :
		d2d(d2d), x(x), y(y), bmp(NULL), drawOrder(drawOrder)
	{
		pixelSize = LoadBMP(d2d, &this->bmp, fileName);
	}

	Sprite::~Sprite()
	{
		this->bmp.ReleaseAndGetAddressOf();
	}


	//Rotates the device context by deg degrees, and saves the center so it can be undone
	void Sprite::Rotate(double deg)
	{
		if ((lastRotate.x == 0) && (lastRotate.y == 0))
		{
			lastRotate.x = x + pixelSize.width / 2;
			lastRotate.y = y + pixelSize.width / 2;

			d2d->devCon->SetTransform(D2D1::Matrix3x2F::Rotation((FLOAT)deg, lastRotate));
		}
	}


	//Draws a given ellipse, with a blue brush and a thickness of 3
	void Sprite::DrawCircle(D2D1_ELLIPSE circle)
	{
		d2d->devCon->DrawEllipse(circle, d2d->blueBrush.Get(), 3.0f);
	}

	//Undoes the previous rotation, if there was one
	void Sprite::UnRotate()
	{
		if ((lastRotate.x == 0) && (lastRotate.y == 0))
		{
			return;
		}

		d2d->devCon->SetTransform(D2D1::Matrix3x2F::Rotation(0, lastRotate));

		lastRotate.x = 0;
		lastRotate.y = 0;
	}

	//Loads a sprite from a bitmap file
	D2D1_SIZE_U Sprite::LoadBMP(Direct2D* d2d, Microsoft::WRL::ComPtr<ID2D1Bitmap1>* src, LPCWSTR fileName)
	{
		Microsoft::WRL::ComPtr<IWICBitmapDecoder> bitmapDecoder;

		d2d->WICFactory->CreateDecoderFromFilename(fileName, NULL, GENERIC_READ, WICDecodeMetadataCacheOnLoad, bitmapDecoder.ReleaseAndGetAddressOf());

		Microsoft::WRL::ComPtr<IWICBitmapFrameDecode> frame;
		bitmapDecoder->GetFrame(0, frame.ReleaseAndGetAddressOf());

		Microsoft::WRL::ComPtr<IWICFormatConverter> image;
		d2d->WICFactory->CreateFormatConverter(image.ReleaseAndGetAddressOf());

		image->Initialize(frame.Get(), GUID_WICPixelFormat32bppPBGRA, WICBitmapDitherTypeNone, NULL, 0, WICBitmapPaletteTypeCustom);

		d2d->devCon->CreateBitmapFromWicBitmap(image.Get(), src->ReleaseAndGetAddressOf());

		return src->Get()->GetPixelSize();
	}

	AnimatedSpriteData::AnimatedSpriteData(Direct2D* d2d, LPCWSTR fileName, std::vector<AnimationInfo> frameData, Chroma chromaEffect) : animationInfo(frameData)
	{
		D2D1_SIZE_U px = Sprite::LoadBMP(d2d, &this->animatedSpriteSheet, fileName);
	}

	AnimatedSpriteData::~AnimatedSpriteData()
	{
		this->animationInfo.clear();
		std::vector<AnimationInfo>(animationInfo).swap(animationInfo);

		animatedSpriteSheet.ReleaseAndGetAddressOf();
	}

	AnimatedSprite::~AnimatedSprite()
	{
		animSpriteData = nullptr;
	}

	AnimatedSprite::AnimatedSprite(Direct2D* d2d, AnimatedSpriteData* animatedSpriteData, float animationFPS,
		float x, float y, unsigned int drawOrd) :
		animSpriteData(animatedSpriteData), animFPS(animationFPS)
	{
		this->bmp = animatedSpriteData->animatedSpriteSheet;
		this->d2d = d2d;
		this->drawOrder = drawOrd;
		this->x = x;
		this->y = y;
		this->currFrame = 0;
	}

	//Sets the sprite's position to the given XY
	void Sprite::SetPosition(float x, float y)
	{
		if ((0 <= x) && (x < (float)DEFAULT_WINDOW_WIDTH) && (0 <= y) && (y < (float)DEFAULT_WINDOW_HEIGHT))
		{
			this->x = x;
			this->y = y;
		}
	}

	//Sets the sprite's location to the given game tile
	void Sprite::SetSquare(int x, int y, D2D1_SIZE_U* drawableSize)
	{
		if ((0 <= x) && (x < (float)TILESWIDTH) && (0 <= y) && (y < TILESHEIGHT))
		{
			this->x = (float)drawableSize ->width/TILESWIDTH *x;
			this->y = (float)drawableSize ->height/TILESHEIGHT *y;
		}
	}

	//Returns a Point, the top left of the sprite
	entities::Point Sprite::GetPosition()
	{
		entities::Point ret(x, y);
		return ret;
	}

	//Draws a sprite with the given rotation from am (optional) source to an (optional) dest
	void Sprite::Draw(double rotation, D2D1_RECT_F* sourceRect, D2D1_RECT_F* destRect, bool flag)
	{
		//If a rotation is set, rotate the device
		if (rotation != 0)
		{
			Rotate(rotation);
		}

		
		if ((!sourceRect) && (!destRect))
		{
			//Draw the source with its literal size
			if (flag)
			{
				D2D1_RECT_F newRect{ this->x, this->y, this->x + pixelSize.width, this->y + pixelSize.height };
				d2d->devCon->DrawBitmap(bmp.Get(), &newRect);
			}

			//Draw the source the same size as a tile
			else
			{
				D2D1_RECT_F rect = { this->x, this->y, this->x + DEFAULT_WINDOW_WIDTH / TILESWIDTH, this->y + DEFAULT_WINDOW_HEIGHT / TILESHEIGHT };
				d2d->devCon->DrawBitmap(bmp.Get(), rect);
			}
			
		}

		//Draw the source into the given rectangle
		else if (!sourceRect)
		{
			d2d->devCon->DrawBitmap(bmp.Get(), destRect);
		}

		//Draw the source from the given rectangle into the given rectangle
		else
		{
			d2d->devCon->DrawBitmap(bmp.Get(), destRect, 1.0f, D2D1_BITMAP_INTERPOLATION_MODE_NEAREST_NEIGHBOR, sourceRect);
		}

		//Un-rotate the device
		if (rotation != 0)
		{
			UnRotate();
		}
	}

	//Finds where to get the source from an animated sprite based on current frame
	//Then draws it
	void AnimatedSprite::Draw()
	{
		unsigned int frame = this->currFrame;
		AnimationInfo data = animSpriteData->animationInfo[0];

		D2D1_RECT_F destRect = { this->x - (data.width*data.rotationCenterX),
								this->y - (data.height*data.rotationCenterY),
								this->x + (data.width*(1.0f - data.rotationCenterX)),
								this->y + (data.height*(1.0f - data.rotationCenterY)) };

		float startX = frame * (data.width + data.paddingWidth) + data.borderPaddingWidth;
		float startY = data.borderPaddingHeight;

		D2D1_RECT_F sourceRect = { startX, startY, startX + data.width, startY + data.height };

		Sprite::Draw(0, &sourceRect, &destRect);
	}

	//Updates the elapsed time for an animated sprite, to see if it needs to be moved to a new frame
	void AnimatedSprite::UpdateTime(double deltaTime)
	{
		frameAge += deltaTime;

		if (frameAge >= (1.0f / (double)animFPS))
		{
			currFrame += (unsigned int)(frameAge * animFPS);

			if (currFrame >= animSpriteData->animationInfo[0].numberOfFrames)
			{
				currFrame = currFrame % animSpriteData->animationInfo[0].numberOfFrames;
			}
		}

		frameAge = std::fmod(frameAge, 1.0f / (double)animFPS);
	}
}