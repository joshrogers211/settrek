/*
*  FILE          : Direct2D.h
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-May-29
*  DESCRIPTION   :
*    Handles some of the base setup for D2D.
*	 Adapted from https://bell0bytes.eu/game-programming/
*/

#pragma once

#include <wrl/client.h>
#include <shared_mutex>

#include <d2d1_3.h>
#include <d3d11_4.h>
#include <dwrite_3.h>

#include <wincodec.h>
#include <WTypes.h>

#pragma comment (lib, "d2d1.lib")
#pragma comment (lib, "d3d11.lib")
#pragma comment (lib, "dwrite.lib")
#pragma comment (lib, "dxguid.lib")
#pragma comment (lib, "Windowscodecs.lib")

class SETTrekGame;

namespace main
{
	class SETTrek;
}

namespace graphics
{
	class Sprite;
	class AnimatedSpriteData;

	

	class Direct2D
	{
	private:
		main::SETTrek* setTrek;

		Microsoft::WRL::ComPtr<IDWriteFactory6> writeFactory;	// pointer to the DirectWrite factory
		Microsoft::WRL::ComPtr<IWICImagingFactory2> WICFactory;	// Windows Imaging Component factory
		Microsoft::WRL::ComPtr<ID2D1Factory7> factory;			// pointer to the Direct2D factory
		Microsoft::WRL::ComPtr<ID2D1Device6> dev;				// pointer to the Direct2D device
		Microsoft::WRL::ComPtr<ID2D1DeviceContext6> devCon;		// pointer to the device context

		// standard colour brushes
		Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> yellowBrush;
		Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> whiteBrush;
		Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> blackBrush;
		Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> redBrush;
		Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> blueBrush;
		Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> brownBrush;
		Microsoft::WRL::ComPtr<ID2D1SolidColorBrush> greenBrush;

		// text formats
		Microsoft::WRL::ComPtr<IDWriteTextFormat3> textFormatPlanetStats;

		// text layouts
		Microsoft::WRL::ComPtr<IDWriteTextLayout4> textLayoutFPS;

		// simple unit geometries

		D2D1::Matrix3x2F matrixTranslation;						// translation matrix
		D2D1::Matrix3x2F matrixRotation;						// rotation matrix
		D2D1::Matrix3x2F matrixScaling;							// scaling matrix
		D2D1::Matrix3x2F matrixShearing;						// shearing matrix

		void CreateDevice();					// creates the device and its context
		void CreateBitmapRenderTarget();		// creates the bitmap render target, set to be the same as the backbuffer already in use for Direct3D
		void CreateDeviceIndependentResources();// creates device independent resources
		void CreateDeviceDependentResources();
		void CreateBrushes();					// initializes different brushes
		void InitializeTextFormats();			// initializes the different formats, for now, only a format to print FPS information will be created


	public:
		Direct2D(main::SETTrek* setTrek);

		~Direct2D();

		friend class main::SETTrek;
		friend class Direct3D;
		friend class SETTrekGame;
		friend class Sprite;
		friend class AnimatedSpriteData;
	};
	
}