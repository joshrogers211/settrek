/*
*  FILE          : Direct3D.cpp
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-May-29
*  DESCRIPTION   :
*    Handles some of the base setup for D3D.
*	 Adapted from https://bell0bytes.eu/game-programming/
*/


#pragma once

#include "Direct3D.h"
#include "SETTrek.h"
#include "Constants.h"

namespace graphics
{
	Direct3D::Direct3D(main::SETTrek* setTrek) : setTrek(setTrek), startInFullscreen(false), 
		currentModeIndex(0), currentlyInFullscreen(false), changeMode(false)
	{
		unsigned int devFlags = D3D11_CREATE_DEVICE_BGRA_SUPPORT;
		D3D_FEATURE_LEVEL featureLevel;

		if (FAILED(D3D11CreateDevice(NULL, D3D_DRIVER_TYPE_HARDWARE, 0, devFlags, NULL, 0, D3D11_SDK_VERSION, &dev,
					&featureLevel, &devCon)))
		{
			throw std::runtime_error("Failed to create D3D device");
		}

		CreateResources();
	}

	Direct3D::~Direct3D()
	{
		swapChain->SetFullscreenState(false, nullptr);
	}

	DXGI_SWAP_CHAIN_DESC Direct3D::CreateSwapChainDesc()
	{
		DXGI_SWAP_CHAIN_DESC swapChainDesc;

		//0 to match the resolution of the output screen
		swapChainDesc.BufferDesc.Width = 0;
		swapChainDesc.BufferDesc.Height = 0;

		//Refresh rate of buffer. 0/1 means we don't care
		swapChainDesc.BufferDesc.RefreshRate.Numerator = 0;
		swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;

		//32 bit colour, 8 bits for each colour channel (RGB) and 8 bits for alpha
		swapChainDesc.BufferDesc.Format = COLOUR_FORMAT;

		//Just use system default scanlines for refreshing screen
		swapChainDesc.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;

		//Defines how the image should be scaled. This will use the system default
		swapChainDesc.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;

		//Number of multisaples per pixel. 1 means to disable MSAA
		swapChainDesc.SampleDesc.Count = 1;

		//Image sample quality. 0 is highest performance, lowest quality (but always supported)
		swapChainDesc.SampleDesc.Quality = 0;

		//Render to backbuffer
		swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;

		//The number of buffers, including the one shown to the user
		swapChainDesc.BufferCount = 3;

		//Gets the window handle for the output target
		swapChainDesc.OutputWindow = setTrek->gameWindow->getGameWindowHandle();

		//Start in windowed mode
		swapChainDesc.Windowed = true;

		//Back-buffer will be swapped to and then discarded
		swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;

		//Allow switching to fullscreen mode
		swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;

		return swapChainDesc;
	}

	void Direct3D::CreateResources()
	{
		//DXGI_SWAP_CHAIN_DESC structure, describes the swap chain for back buffering
		DXGI_SWAP_CHAIN_DESC swapChainDesc = CreateSwapChainDesc();

		//Get DXGI device factory
		Microsoft::WRL::ComPtr<IDXGIDevice> dxgiDevice;
		Microsoft::WRL::ComPtr<IDXGIAdapter> dxgiAdapter;
		Microsoft::WRL::ComPtr<IDXGIFactory> dxgiFactory;

		//Retrieve the DXGI device
		dev.As(&dxgiDevice);

		//Get address of GPU
		dxgiDevice->GetAdapter(dxgiAdapter.GetAddressOf());

		//Retrieve the DXGI factory (holy moly this is so much work why)
		dxgiAdapter->GetParent(__uuidof(IDXGIFactory), &dxgiFactory);

		//Hey we're finally creating that swapchain we defined above!
		dxgiFactory->CreateSwapChain(dev.Get(), &swapChainDesc, swapChain.GetAddressOf());

		//Output adapter for the swampchain
		IDXGIOutput *output = nullptr;
		swapChain->GetContainingOutput(&output);

		//Gets a list of valid display modes on this monitor
		output->GetDisplayModeList(COLOUR_FORMAT, 0, &numberOfSupportedModes, NULL);

		//Set up the array that stores available display modes
		supportedModes = new DXGI_MODE_DESC[numberOfSupportedModes];
		ZeroMemory(supportedModes, sizeof(DXGI_MODE_DESC) * numberOfSupportedModes);

		//Fills the display mode array
		output->GetDisplayModeList(COLOUR_FORMAT, 0, &numberOfSupportedModes, supportedModes);

		//Output adapter no longer needed
		output->Release();
	}

	void Direct3D::OnResize()
	{
		//Zero refresh rate before resizing window
		DXGI_MODE_DESC emptyRefreshRate = currentModeDescription;
		emptyRefreshRate.RefreshRate.Numerator = 0;
		emptyRefreshRate.RefreshRate.Denominator = 0;

		BOOL inFullscreen = false;
		swapChain->GetFullscreenState(&inFullscreen, NULL);

		if (currentlyInFullscreen != inFullscreen)
		{
			if (inFullscreen)
			{
				swapChain->ResizeTarget(&emptyRefreshRate);

				swapChain->SetFullscreenState(true, nullptr);

				currentlyInFullscreen = true;
			}
			else
			{
				swapChain->SetFullscreenState(false, nullptr);

				RECT rect = { 0, 0, (long)setTrek->d3d->currentModeDescription.Width,  (long)setTrek->d3d->currentModeDescription.Height };
				AdjustWindowRectEx(&rect, WS_OVERLAPPEDWINDOW, false, WS_EX_OVERLAPPEDWINDOW);

				currentlyInFullscreen = false;
			}
		}

		swapChain->ResizeTarget(&emptyRefreshRate);

		//Release device context resources
		if (setTrek->d2d)
		{
			setTrek->d2d->devCon->SetTarget(nullptr);
		}

		//Resets device context to default
		devCon->ClearState();
		renderTargetView = nullptr;
		depthStencilView = nullptr;

		//Resize swap chain
		swapChain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH);

		//Create the render target view
		Microsoft::WRL::ComPtr<ID3D11Texture2D> backBuffer;
		swapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(backBuffer.GetAddressOf()));
		dev->CreateRenderTargetView(backBuffer.Get(), NULL, &renderTargetView);

		// create the depth and stencil buffer
		D3D11_TEXTURE2D_DESC dsd;
		Microsoft::WRL::ComPtr<ID3D11Texture2D> dsBuffer;
		backBuffer->GetDesc(&dsd);
		dsd.Format = DXGI_FORMAT_R8G8B8A8_SNORM;
		dsd.Usage = D3D11_USAGE_DEFAULT;
		dsd.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		dev->CreateTexture2D(&dsd, NULL, dsBuffer.ReleaseAndGetAddressOf());
		dev->CreateDepthStencilView(dsBuffer.Get(), NULL, depthStencilView.GetAddressOf());

		//Activate the depth and stencil buffer
		devCon->OMSetRenderTargets(1, renderTargetView.GetAddressOf(), depthStencilView.Get());

		D3D11_VIEWPORT vp;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;
		vp.Width = (float)dsd.Width;
		vp.Height = (float)dsd.Height;
		vp.MinDepth = 0.0f;
		vp.MaxDepth = 1.0f;
		devCon->RSSetViewports(1, &vp);

		// re-create Direct2D device dependent resources
		if (setTrek->d2d)
		{
			setTrek->d2d->CreateBitmapRenderTarget();
			setTrek->d2d->CreateDeviceDependentResources();
		}

		// re-initialize GPU pipeline
		InitPipeline();

		return;
	}

	//Loads vertex and pixel shaders
	ShaderBuffer Direct3D::LoadShader(std::wstring filename)
	{
		ShaderBuffer retBuff;
		byte* shaderData = nullptr;

		std::ifstream shaderFile(filename, std::ios::in | std::ios::binary | std::ios::ate);

		//If file opened successfully, load data
		if (shaderFile.is_open())
		{
			retBuff.size = (unsigned int)shaderFile.tellg();

			shaderData = new byte[retBuff.size];
			shaderFile.seekg(0, std::ios::beg);
			shaderFile.read(reinterpret_cast<char*>(shaderData), retBuff.size);

			shaderFile.close();

			//Store data in the shaderbuffer buffer
			retBuff.buffer = shaderData;
		}

		return retBuff;
	}


	//Initialize the graphics pipeline with shaders
	void Direct3D::InitPipeline()
	{
		//These should be constants but yolo
		ShaderBuffer vertexShaderBuffer = LoadShader(L"../x64/Debug/vertexShader.cso");
		ShaderBuffer pixelShaderBuffer = LoadShader(L"../x64/Debug/pixelShader.cso");

		dev->CreateVertexShader(vertexShaderBuffer.buffer, vertexShaderBuffer.size, nullptr, &standardVertexShader);
		dev->CreatePixelShader(pixelShaderBuffer.buffer, pixelShaderBuffer.size, nullptr, &standardPixelShader);

		devCon->VSSetShader(standardVertexShader.Get(), nullptr, 0);
		devCon->PSSetShader(standardPixelShader.Get(), nullptr, 0);

		D3D11_INPUT_ELEMENT_DESC ied[] = { { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
											{ "COLOR", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 12, D3D11_INPUT_PER_VERTEX_DATA, 0 } };

		Microsoft::WRL::ComPtr<ID3D11InputLayout> inputLayout;

		dev->CreateInputLayout(ied, ARRAYSIZE(ied), vertexShaderBuffer.buffer, vertexShaderBuffer.size, &inputLayout);
		devCon->IASetInputLayout(inputLayout.Get());

		D3D11_BUFFER_DESC bd = { 0 };
		bd.ByteWidth = 32;
		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;

		dev->CreateBuffer(&bd, nullptr, &constantColourPositionBuffer);
		devCon->VSSetConstantBuffers(0, 1, constantColourPositionBuffer.GetAddressOf());

		delete vertexShaderBuffer.buffer;
		delete pixelShaderBuffer.buffer;

		return;
	}

	void Direct3D::ClearBuffers()
	{
		// clear the Direct2D render target

		// clear the back buffer and depth / stencil buffer
		float black[] = { 0.0f, 0.0f, 0.0f, 0.0f };
		float white[] = { 1.0f, 1.0f, 1.0f, 1.0f };
		devCon->ClearRenderTargetView(renderTargetView.Get(), white);
		devCon->ClearDepthStencilView(depthStencilView.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);
	}

	int Direct3D::Present()
	{
		HRESULT hr = swapChain->Present(0, DXGI_PRESENT_DO_NOT_WAIT);
		if (FAILED(hr) && hr != DXGI_ERROR_WAS_STILL_DRAWING)
		{
			return -1;
		}

		// rebind the depth and stencil buffer - necessary since the flip model releases the view targets after a call to present
		devCon->OMSetRenderTargets(1, renderTargetView.GetAddressOf(), depthStencilView.Get());

		// return success
		return 0;
	}
}