/*
*  FILE          : SETTrekGame.cpp
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-May-29
*  DESCRIPTION   :
*    Most of the game logic for SETTrek. Initializes assets, runs loop
*/

#include "SETTrekGame.h"

//Nothing extra needs to be done over SETTrek::RunGame()
int SETTrekGame::RunGame()
{
	return SETTrek::RunGame();
}

//Nothing extra needs to be done over SETTrek::OnResize()
void SETTrekGame::OnResize()
{
	SETTrek::OnResize();
}

//Updates the game world based on the elapsed time (dt)
int SETTrekGame::UpdateGameWorld(double dt)
{
	if (playerShip->GetEnergy() <= 0)
	{
		run = false;
		return 0;
	}
	//If the player is on a planet, advance the planet spinning animation
	if (playerShip->onPlanet)
	{
		rotatingPlanet->UpdateTime(dt);
	}
	//If the player is not on a planet, stuff happens in space
	else
	{
		//If the missile exists, it will chase the enemy
		missile->UpdateLocation(dt);

		//Update the player location (if it's moving)
		playerShip->UpdateLocation(dt);

		//Check if the player has landed on any planets
		playerShip->CheckPlanetCollision(planets);

		//Move the enemy ship towards the player ship
		enemyShip->UpdateLocation(playerShip, dt);

		//Check for collision between player and enemy ship
		switch (playerShip->CheckEnemyCollision(enemyShip->GetLocation()))
		{
		//If the player didn't have shields
		case 1:
			//If the player is out of energy, the game ends
			if (playerShip->GetEnergy() == 0)
			{
				run = false;
				return 0;
			}

			//Else, make a new level
			NewLevel();
			break;

		//If the player does have shields
		case 2:
			//Play shield breaking sound and reset the enemy
			mciSendString("play shieldBreak.wav", NULL, 0, NULL);
			enemyShip->Reset();
			break;
		}
	}

	return 0;
}

void SETTrekGame::EndScreen()
{
	run = false;

	d3d->ClearBuffers();

	d2d->devCon->BeginDraw();

	std::string scString = "Game over. You got " + std::to_string(playerShip->GetScience()) + " science!";

	std::wstring scienceStr(scString.begin(), scString.end());
	d2d->writeFactory->CreateTextLayout(scienceStr.c_str(), (UINT32)scienceStr.size(), d2d->textFormatPlanetStats.Get(), DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, (IDWriteTextLayout **)d2d->textLayoutFPS.GetAddressOf());
	d2d->devCon->DrawTextLayout(D2D1::Point2F(END_SCIENCE_TEXT_X, END_SCIENCE_TEXT_Y), d2d->textLayoutFPS.Get(), d2d->whiteBrush.Get());

	d2d->devCon->EndDraw();

	d3d->Present();
}


int SETTrekGame::Render(double /*farSeer*/)
{
	d3d->ClearBuffers();

	d2d->devCon->BeginDraw();

	d2d->matrixScaling = D2D1::Matrix3x2F::Scale(3, 3, D2D1::Point2F(400, 300));

	D2D1_RECT_F rect = { 0, 0, (float) drawableSize.width, (float) drawableSize.height };

	bg->Draw(NULL, &rect);

	for (auto p : planets)
	{
		p->Draw();
	}

	enemyShip->Draw();
	playerShip->Draw();
	missile->Draw();


	if (playerShip->onPlanet)
	{
		planetMenu->Draw(NULL, &rect);

		rotatingPlanet->Draw();
	}

	WriteStrings();

	d2d->devCon->EndDraw();

	d3d->Present();

	return 0;
}

void SETTrekGame::WriteStrings()
{
	if (playerShip->onPlanet)
	{
		//planetMenu->Draw(NULL, &rect);

		std::string enString = playerShip->GetPEnergy();
		std::string scString = playerShip->GetPScience();

		std::wstring energyStr(enString.begin(), enString.end());

		d2d->writeFactory->CreateTextLayout(energyStr.c_str(), (UINT32)energyStr.size(), d2d->textFormatPlanetStats.Get(), DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, (IDWriteTextLayout **)d2d->textLayoutFPS.GetAddressOf());
		d2d->devCon->DrawTextLayout(D2D1::Point2F(ENERGY_TEXT_X, ENERGY_TEXT_Y), d2d->textLayoutFPS.Get(), d2d->blackBrush.Get());

		std::wstring scienceStr(scString.begin(), scString.end());
		d2d->writeFactory->CreateTextLayout(scienceStr.c_str(), (UINT32)scienceStr.size(), d2d->textFormatPlanetStats.Get(), DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, (IDWriteTextLayout **)d2d->textLayoutFPS.GetAddressOf());
		d2d->devCon->DrawTextLayout(D2D1::Point2F(SCIENCE_TEXT_X, SCIENCE_TEXT_Y), d2d->textLayoutFPS.Get(), d2d->blackBrush.Get());
	}

	std::string playerEnString = "Energy: " + std::to_string(playerShip->GetEnergy());
	std::string playerScString = "Science: " + std::to_string(playerShip->GetScience());

	std::wstring pScienceStr(playerScString.begin(), playerScString.end());
	d2d->writeFactory->CreateTextLayout(pScienceStr.c_str(), (UINT32)pScienceStr.size(), d2d->textFormatPlanetStats.Get(), DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, (IDWriteTextLayout **)d2d->textLayoutFPS.GetAddressOf());
	d2d->devCon->DrawTextLayout(D2D1::Point2F(PLAYER_SCIENCE_X, PLAYER_SCIENCE_Y), d2d->textLayoutFPS.Get(), d2d->whiteBrush.Get());

	std::wstring pEnergyStr(playerEnString.begin(), playerEnString.end());
	d2d->writeFactory->CreateTextLayout(pEnergyStr.c_str(), (UINT32)pEnergyStr.size(), d2d->textFormatPlanetStats.Get(), DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT, (IDWriteTextLayout **)d2d->textLayoutFPS.GetAddressOf());
	d2d->devCon->DrawTextLayout(D2D1::Point2F(PLAYER_ENERGY_X, PLAYER_ENERGY_Y), d2d->textLayoutFPS.Get(), d2d->whiteBrush.Get());
}

SETTrekGame::SETTrekGame(HINSTANCE hInstance) : SETTrek(hInstance) {}
SETTrekGame::~SETTrekGame() {};

void SETTrekGame::InitGame()
{
	SETTrek::InitGame();
	InitGraphics();
	srand((unsigned int)time(0));
	return;
}

void SETTrekGame::OnKeyDown(WPARAM wParam, LPARAM lParam)
{
	int choice = (int)wParam;
	if ((choice == CHOICE_GET_SCIENCE) || (choice == CHOICE_GET_ENERGY) || (choice == CHOICE_LEAVE_PLANET))
	{
		playerShip->ExplorePlanet(choice);
	}

	if (choice == MISSILE_BUTTON)
	{
		missile->FireMissile(playerShip, enemyShip);
	}
}

void SETTrekGame::OnClickParent(int x, int y)
{
	playerShip->SetTarget(entities::Point((float)x, (float)y), drawableSize);
}

void SETTrekGame::NewLevel()
{
	playerShip->Reset();
	enemyShip->Reset();

	switch (rand() % (NUM_DIFF_PLANETS)+1)
	{
	case 1:
		PlaySound(MAIN_THEME_1, NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
		break;
	case 2:
		PlaySound(MAIN_THEME_2, NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
		break;
	default:
		PlaySound(MAIN_THEME_3, NULL, SND_FILENAME | SND_ASYNC | SND_LOOP);
	}
	

	planets.clear();

	//Fill the world with planets 
	for (int i = 0; i < TILESWIDTH; i++)
	{
		for (int j = 0; j < TILESHEIGHT; j++)
		{
			if ((i != PLAYER_START_X) && (j != PLAYER_START_Y) && (i != ENEMY_START_X) && (j != ENEMY_START_Y))
			{
				if ((rand() % (100) + 1) <= PLANET_PERCENT_CHANCE)
				{
					switch (rand() % (NUM_DIFF_PLANETS)+1)
					{
					case 1:
						p1->SetSquare(i, j, &drawableSize);
						planets.push_back(new entities::Planet(p1));
						break;
					case 2:
						p2->SetSquare(i, j, &drawableSize);
						planets.push_back(new entities::Planet(p2));
						break;
					default:
						p3->SetSquare(i, j, &drawableSize);
						planets.push_back(new entities::Planet(p3));
					}
				}
			}
		}
	}

	mciSendString("play warp.wav", NULL, 0, NULL);
}

void SETTrekGame::InitGraphics()
{
	drawableSize = d2d->devCon->GetPixelSize();

	

	//player ship
	playerShip = new entities::PlayerShip(new graphics::Sprite(d2d, PLAYER_SHIP_FILE, 0.0f, 0.0f, graphics::Chroma::GREEN, true, 99));
	//spriteMap.insert(std::make_pair(PLAYER_SHIP, new graphics::Sprite(d2d, PLAYER_SHIP_FILE, 0.0f, 0.0f, graphics::Chroma::GREEN, true, 99)));

	//enemy ship
	enemyShip = new entities::EnemyShip(
		new graphics::Sprite(d2d, ENEMY_SHIP_FILE, 0.0f, 0.0f, graphics::Chroma::BLUE, true, 98),
		new graphics::Sprite(d2d, ENEMY_SHIP_FILE_D, 0.0f, 0.0f, graphics::Chroma::BLUE, true, 98));

	missile = new entities::Missile(new graphics::Sprite(d2d, MISSILE_FILE, 0.0f, 0.0f, graphics::Chroma::BLUE, true, 98));

	//background
	bg = new graphics::Sprite(d2d, GAME_BACKGROUND_FILE, 0.0f, 0.0f);

	planetMenu = new graphics::Sprite(d2d, PLANET_MENU_FILE, 0.0f, 0.0f);

	//Load planet BMPs
	p1 = new graphics::Sprite(d2d, PLANET_ONE_FILE, 0.0f, 0.0f, graphics::Chroma::GREEN, true, 1);
	p2 = new graphics::Sprite(d2d, PLANET_TWO_FILE, 0.0f, 0.0f, graphics::Chroma::GREEN, true, 1);
	p3 = new graphics::Sprite(d2d, PLANET_THREE_FILE, 0.0f, 0.0f, graphics::Chroma::GREEN, true, 1);

	//rotating planet
	std::vector<graphics::AnimationInfo> spinningPlanetCycles;
	graphics::AnimationInfo spinningPlanet;

	spinningPlanet.name = ROTATING_PLANET_NAME;
	spinningPlanet.startFrame = 0;
	spinningPlanet.numberOfFrames = ROTATING_PLANET_NUM_FRAMES;
	spinningPlanet.width = ROTATING_PLANET_WIDTH;
	spinningPlanet.height = ROTATING_PLANET_HEIGHT;
	spinningPlanet.paddingWidth = ROTATING_PLANET_PADDING;
	spinningPlanet.paddingHeight = ROTATING_PLANET_PADDING;
	spinningPlanet.borderPaddingHeight = spinningPlanet.borderPaddingWidth = 5; //Possibly change later
	spinningPlanet.rotationCenterX = spinningPlanet.rotationCenterY = 0.5f;
	spinningPlanetCycles.push_back(spinningPlanet);

	threeDPlanet = new graphics::AnimatedSpriteData(d2d, ROTATING_PLANET_FILE, spinningPlanetCycles, graphics::Chroma::GREEN);

	rotatingPlanet = new graphics::AnimatedSprite(d2d, threeDPlanet, ANIMATION_FPS, ROTATING_PLANET_X, ROTATING_PLANET_Y);

	NewLevel();
}

void SETTrekGame::Cleanup()
{
	delete threeDPlanet;
	delete rotatingPlanet;
	delete enemyShip;
	delete playerShip;
	delete p1;
	delete p2;
	delete p3;

}