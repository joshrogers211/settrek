/*
*  FILE          : Point.h
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Mar-12
*  DESCRIPTION   :
*    Point and vector math for player movement and distance
*/

#pragma once
#include <math.h>

namespace entities
{
	class Vector
	{
	public:
		float x, y;

		Vector() {}

		Vector(float newX, float newY);

		//Finds the length of a vector
		float Length() const;

		//Scales a vector by a given factor
		Vector operator*(float s) const;

		//Scales a vector by a given factor
		Vector operator/(float s) const;
	};

	class Point
	{
	public:
		float x, y;

		Point() {}
		Point(float newX, float newY);

		//Adds a vector to a point
		Point AddVector(Vector v);

		bool operator==(Point a);
		friend Vector operator-(Point a, Point b);
	};
}