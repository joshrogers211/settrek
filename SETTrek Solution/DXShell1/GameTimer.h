#pragma once


namespace main
{
	class GameTimer
	{
		long long int startTime;			// time at the start of the application
		long long int currentTime;			// stores the current time; i.e. time at the current frame
		long long int previousTime;		    // stores the time at the last inquiry before current; i.e. time at the previous frame
		long long int pausedTime;
		long long int totalPausedTime;

		double secondsPerTick;
		double deltaTime;

		bool isRunning;

	public:
		GameTimer();

		double GetDeltaTime() const;

		void Start();
		void Stop();
		void Tick();
		void Reset();
	};
}