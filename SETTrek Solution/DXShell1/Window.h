/*
*  FILE          : Windows.h
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Feb-11
*  DESCRIPTION   :
*    Handles windows messages and initialization
*/

#pragma once
#include <Windows.h>

namespace main
{
	class SETTrek;

	class Window
	{
	private:
		HWND gameWindow;
		SETTrek* setTrek;

		unsigned int gameWidth;
		unsigned int gameHeight;

		int initializeWindow();

	public:
		Window(SETTrek* setTrek);
		~Window();

		inline HWND getGameWindowHandle() const { return gameWindow; };

		void SendUserMessage(unsigned int msg, int param) const;

		virtual LRESULT CALLBACK msgProc(HWND hWnd, unsigned int msg, WPARAM wParam, LPARAM lParam);

		friend class SETTrek;
	};
}