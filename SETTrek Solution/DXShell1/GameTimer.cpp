/*
*  FILE          : GameTimer.cpp
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Apr-01
*  DESCRIPTION   :
*    Functions relating to game time
*/

#include <Windows.h>
#include "GameTimer.h"

namespace main
{
	GameTimer::GameTimer()
	{
		startTime = 0;
		currentTime = 0;
		previousTime = 0;
		secondsPerTick = 0.0f;
		deltaTime = 0.0f;
		isRunning = true;

		long long int frequency = 0;

		if (QueryPerformanceFrequency((LARGE_INTEGER*)&frequency))
		{
			secondsPerTick = 1.0 / (double)frequency;
		}
	}

	//Accessor
	double GameTimer::GetDeltaTime() const
	{
		return deltaTime;
	}

	//Starts the game timer
	void GameTimer::Start()
	{
		if (!isRunning)
		{
			long long int now = 0;
			if (QueryPerformanceCounter((LARGE_INTEGER*)&now))
			{
				totalPausedTime += (now - pausedTime);
				previousTime = now;

				pausedTime = 0;
				isRunning = true;

				return;
			}
		}
	}

	//Stops/pauses the game timer
	void GameTimer::Stop()
	{
		if (isRunning)
		{
			long long int now = 0;
			if (QueryPerformanceCounter((LARGE_INTEGER*)&now))
			{
				pausedTime = now;

				isRunning = false;
			}
		}
	}

	//Calculates the time since last tick
	void GameTimer::Tick()
	{
		if (isRunning)
		{
			if (QueryPerformanceCounter((LARGE_INTEGER*)&currentTime))
			{
				deltaTime = (currentTime - previousTime) * secondsPerTick;

				previousTime = currentTime;

				if (deltaTime < 0.0f)
				{
					deltaTime = 0.0f;
				}

				return;
			}
			else
			{
				deltaTime = 0.0f;

				return;
			}
		}
	}

	//Resets the game timer
	void GameTimer::Reset()
	{
		long long int now = 0;

		if (QueryPerformanceCounter((LARGE_INTEGER*)&now))
		{
			startTime = now;
			previousTime = now;
			pausedTime = 0;
			isRunning = true;

			return;
		}
	}
}