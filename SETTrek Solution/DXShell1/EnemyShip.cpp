/*
*  FILE          : EnemyShip.cpp
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Feb-11
*  DESCRIPTION   :
*    Functions related to controlling the enemy ship for SETTrek
*		handles movement, damage, and drawing
*/

#include "EnemyShip.h"

namespace entities
{
	EnemyShip::EnemyShip(graphics::Sprite* shipSpriteHealthy, graphics::Sprite* shipSpriteDamaged) : enemySpriteHealthy(shipSpriteHealthy), enemeySpriteDamaged(shipSpriteDamaged)
	{
		D2D1_SIZE_U temp{ DEFAULT_WINDOW_WIDTH, DEFAULT_WINDOW_HEIGHT };
		enemySpriteHealthy->SetSquare(ENEMY_START_X, ENEMY_START_Y, &temp);
		enemeySpriteDamaged->SetSquare(ENEMY_START_X, ENEMY_START_Y, &temp);
		angleToTarget = 0;
		damaged = false;

		shipLocation = enemySpriteHealthy->GetPosition();
		defaultLocation = shipLocation;
	}

	//Calculate the angle to rotate to face the player
	void EnemyShip::CalcTargetAngle()
	{
		//If we're at the player, don't calc
		if (playerTarget == shipLocation)
		{
			angleToTarget = 0;
			return;
		}

		//Calc vector between player and enemy
		Vector v = playerTarget - shipLocation;

		//Use trig to calc angle
		angleToTarget = atan2(v.y , v.x) * 180 / PI + 180;
	}

	//Moves the enemy towards the player <distance> pixels
	void EnemyShip::MoveEnemy(float distance)
	{
		Point newPos;

		//Calculate the X and Y portions of the movement
		newPos.x = shipLocation.x + distance * (float) cos((angleToTarget - 180) * PI / 180);
		newPos.y = shipLocation.y + distance * (float) sin((angleToTarget - 180) * PI / 180);

		shipLocation = newPos;
	}

	//Updates the enemy ship's location, based on player location and elapsed time
	void EnemyShip::UpdateLocation(PlayerShip* player, double dt)
	{
		//Only move if the player is moving
		if (player->isMoving)
		{
			//Gets player location and calcs distance
			playerTarget = player->GetLocation();
			Vector v = playerTarget - shipLocation;
			float distance = v.Length();

			//How far to move, based on defined speed and elapsed time
			float pixelsToMove = (float) dt * ENEMY_SPEED;

			//If enemy is close, gain a speed boost
			if (distance <= ENEMY_SPEED_DISTANCE)
			{
				pixelsToMove *= ENEMY_SPEED_INCREASE;
			}

			//Recalc target angle
			CalcTargetAngle();

			//Move
			MoveEnemy(pixelsToMove);
		}
	}

	//Returns the enemy ship's loation as a point
	Point EnemyShip::GetLocation()
	{
		return shipLocation;
	}

	//Resets the ship to its default location and damage state
	void EnemyShip::Reset()
	{
		damaged = false;
		angleToTarget = 0;
		shipLocation = defaultLocation;
	}

	//Either destroy/reset the ship, or flag it as damaged if undamaged
	void EnemyShip::TakeDamage()
	{
		mciSendString("play enemyExpl.wav", NULL, 0, NULL);

		if (damaged)
		{
			Reset();
		}
		else
		{
			damaged = true;
		}
	}

	//Draws either the damaged or healthy ship, depending on the damaged flag
	void EnemyShip::Draw()
	{
		if (damaged)
		{
			enemeySpriteDamaged->SetPosition(shipLocation.x, shipLocation.y);
			enemeySpriteDamaged->Draw(std::floor(angleToTarget + 180));
		}
		else
		{
			enemySpriteHealthy->SetPosition(shipLocation.x, shipLocation.y);
			enemySpriteHealthy->Draw(std::floor(angleToTarget + 180));
		}
	}
}