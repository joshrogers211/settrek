/*
*  FILE          : SETTrekGame.h
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Mar-26
*  DESCRIPTION   :
*    Handles the creation of assets, updating all entities,
*	 and the main game loop
*/

#pragma once
#include <Windows.h>
#include <map>
#include <vector>
#include <list>
#include <string>

#include "SETTrek.h"
#include "Graphics.h"

#include "Point.h"
#include "Planet.h"
#include "PlayerShip.h"
#include "EnemyShip.h"
#include "Missile.h"

class SETTrekGame : main::SETTrek
{
	graphics::AnimatedSpriteData* threeDPlanet;
	graphics::AnimatedSprite* rotatingPlanet;


	std::list <entities::Planet*> planets;
	entities::PlayerShip* playerShip;
	entities::EnemyShip* enemyShip;
	entities::Missile* missile;
	graphics::Sprite* bg;
	graphics::Sprite* planetMenu;

	graphics::Sprite* p1;
	graphics::Sprite* p2;
	graphics::Sprite* p3;

	D2D1_SIZE_U drawableSize;

public:
	SETTrekGame(HINSTANCE hInstance);
	~SETTrekGame();

	virtual void OnKeyDown(WPARAM wParam, LPARAM lParam);
	virtual void OnClickParent(int x, int y);

	void NewLevel();
	void InitGame() override;
	void Cleanup() override;
	int UpdateGameWorld(double dt);
	int Render(double farSeer);
	void OnResize();
	void WriteStrings();

	void EndScreen();

	void InitGraphics();

	int RunGame() override;
};