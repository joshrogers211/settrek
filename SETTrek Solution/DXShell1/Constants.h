/*
*  FILE          : Constants.h
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-May-29
*  DESCRIPTION   :
*    Long list of constants to avoid magic numbers.
*	 Basically anything you want can be tweaked here!
*/

#pragma once

#define PI 3.14159

#define END_SCREEN_VAL 1337

#define DEFAULT_WINDOW_WIDTH 1024
#define DEFAULT_WINDOW_HEIGHT 768
#define TILESWIDTH 10
#define TILESHEIGHT 10

#define PLAYER_SPEED 120 //In pixels per second
#define ENEMY_SPEED 115 //In pixels per second
#define ENEMY_SPEED_INCREASE 1.1f //Percent increase when enemy is close to player
#define ENEMY_SPEED_DISTANCE DEFAULT_WINDOW_WIDTH/TILESWIDTH*2 //Distance away at which the enemy gets a speed boost

#define PLAYER_COLLISION_CIRCLE DEFAULT_WINDOW_WIDTH/TILESWIDTH/2.0f

#define PLANET_EXPLORATION_MENU_BORDER 50 //in pixels

#define PLAYER_START_X 0		//Player start location in tiles
#define PLAYER_START_Y 4.5f

#define SECONDS_PER_FRAME 1/(double)60

#define ENEMY_START_X TILESWIDTH-1	//Enemy start location in tiles
#define ENEMY_START_Y 5

#define PLAYER_START_ENERGY 200
#define PLAYER_MAX_ENERGY 900
#define PLAYER_DEATH_ENERGY_LOSS 300

#define MISSILE_SPEED 500
#define MISSILE_EXPL_DIST 10
#define MISSILE_BUTTON 0x4D
#define MISSILE_ENERGY_COST 20

#define PLANET_MAX_SCIENCE 300
#define PLANET_MIN_SCIENCE 0

#define PLANET_MAX_ENERGY 300
#define PLANET_MIN_ENERGY 0

#define PLANET_PERCENT_CHANCE 5		//% chance that a tile contains a planet

#define MAIN_THEME_1 "main1.wav"	//Filenames of main menu music
#define MAIN_THEME_2 "main2.wav"
#define MAIN_THEME_3 "main3.wav"

#define CHOICE_GET_SCIENCE	0x31	//1 key
#define CHOICE_GET_ENERGY	0x32	//2 key
#define CHOICE_LEAVE_PLANET 0x33	//3 key

#define ENERGY_TEXT_X 590.0F		//Location of text on planet exploration menu
#define ENERGY_TEXT_Y 265.0F
#define SCIENCE_TEXT_X 590.0f
#define SCIENCE_TEXT_Y 230.0f

#define END_SCIENCE_TEXT_X	200.0f
#define END_SCIENCE_TEXT_Y 200.0f

#define PLAYER_SCIENCE_X 5.0f		//Location of text for player stats
#define PLAYER_SCIENCE_Y 5.0f
#define PLAYER_ENERGY_X 10.0f
#define PLAYER_ENERGY_Y 35.0f

#define WINDOW_TITLE	"SETTrek"	//Window title
#define ANIMATION_FPS	30.0f		//framerate of animated sprites

#define GAME_BACKGROUND			0
#define GAME_BACKGROUND_FILE	L"SectorBackground.bmp"

#define PLANET_MENU			1
#define PLANET_MENU_FILE	L"planetMenu.png"

#define PLAYER_SHIP			2
#define PLAYER_SHIP_FILE	L"Ship.png"

#define ENEMY_SHIP			3
#define ENEMY_SHIP_FILE		L"EnemyShip.png"
#define ENEMY_SHIP_FILE_D	L"EnemyShipDamaged.png"

#define PLANET_ONE			4
#define PLANET_ONE_FILE		L"Planet1.png"

#define PLANET_TWO			5	//Probably not going to be used, but reserved
#define PLANET_TWO_FILE		L"Planet2.png"

#define PLANET_THREE		6	//Probably not going to be used, but reserved
#define PLANET_THREE_FILE	L"Planet3.png"

#define NUM_DIFF_PLANETS	3

#define MISSILE				7
#define MISSILE_FILE		L"Missile.png"

#define ROTATING_PLANET_NAME		L"Rotating Planet"
#define ROTATING_PLANET_FILE		L"RotatingPlanetSheet.png"
#define ROTATING_PLANET_NUM_FRAMES	89
#define ROTATING_PLANET_PADDING		10
#define ROTATING_PLANET_HEIGHT		128
#define ROTATING_PLANET_WIDTH		128
#define ROTATING_PLANET_X			380.0f
#define ROTATING_PLANET_Y			239.0f

#define COLOUR_FORMAT DXGI_FORMAT_B8G8R8A8_UNORM

