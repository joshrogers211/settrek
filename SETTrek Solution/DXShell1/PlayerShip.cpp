/*
*  FILE          : PlayerShip.cpp
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Mar-12
*  DESCRIPTION   :
*    Handles player ship movement and drawing. planet exploration, energy/science tracking
*/

#pragma once
#include "PlayerShip.h"

namespace entities
{
	PlayerShip::PlayerShip(graphics::Sprite *shipSprite) : playerSprite(shipSprite)
	{
		playerLocation = Point(DEFAULT_WINDOW_WIDTH/TILESWIDTH*PLAYER_START_X, (float)abs(DEFAULT_WINDOW_HEIGHT/TILESHEIGHT*(PLAYER_START_Y - 0.5)));
		targetLocation = playerLocation;
		defaultLocation = playerLocation;
		isMoving = false;
		hasShields = true;
		onPlanet = false;
		energy = PLAYER_START_ENERGY;
		science = 0;
		angleToTarget = 0;
		shipSprite->SetPosition(playerLocation.x, playerLocation.y);
	}

	void PlayerShip::Reset()
	{
		angleToTarget = 0;

		hasShields = true;

		targetLocation = defaultLocation;
		playerLocation = defaultLocation;
	}

	int PlayerShip::GetScience()
	{
		return science;
	}

	int PlayerShip::GetEnergy()
	{
		return energy;
	}

	void PlayerShip::SetTarget(Point newTarget, D2D1_SIZE_U drawableArea)
	{
		if ((newTarget.x <= drawableArea.width) && (newTarget.y <= drawableArea.height))
		{
			if (!onPlanet)
			{
				targetLocation = newTarget;
				CalcTargetAngle();
			}
			else
			{
				lastClick = newTarget;
			}
		}
	}

	//Calculates the angle to the target destination (for sprite rotation and movement)
	void PlayerShip::CalcTargetAngle()
	{
		if (playerLocation == targetLocation)
		{
			angleToTarget = 0;
		}
		else 
		{
			Vector v = targetLocation - playerLocation;
			angleToTarget = atan2(v.y , v.x) * 180 / PI;
		}
	}

	//Returns player location
	Point PlayerShip::GetLocation()
	{
		return playerLocation;
	}

	//Draws the ship. If it has shields, creates a circle and draws that too
	void PlayerShip::Draw()
	{
		playerSprite->SetPosition(playerLocation.x, playerLocation.y);
		playerSprite->Draw(std::floor(angleToTarget));

		if (hasShields)
		{
			D2D1_POINT_2F shieldCentre{ playerLocation.x + DEFAULT_WINDOW_WIDTH / TILESWIDTH / 2, playerLocation.y + DEFAULT_WINDOW_HEIGHT / TILESHEIGHT / 2 };

			D2D1_ELLIPSE ellipse = D2D1::Ellipse(
				shieldCentre,
				DEFAULT_WINDOW_WIDTH / TILESWIDTH / 2 + 15,
				DEFAULT_WINDOW_HEIGHT / TILESHEIGHT / 2 + 15);

			playerSprite->DrawCircle(ellipse);
		}
	}

	//If a missile was fired, decrement player energy
	void PlayerShip::MissileFired()
	{
		energy -= MISSILE_ENERGY_COST;
	}

	//Handles planet exploration commands
	void PlayerShip::ExplorePlanet(int choice)
	{
		if ((onPlanet) && (currPlanet != nullptr))
		{
			switch (choice)
			{
			case CHOICE_GET_SCIENCE:
				science += currPlanet->science;
				currPlanet->science = 0;
				break;
			case CHOICE_GET_ENERGY:
				energy += currPlanet->energy;
				if (energy > PLAYER_MAX_ENERGY)
				{
					energy = PLAYER_MAX_ENERGY;
				}
				currPlanet->energy = 0;
				break;
			case CHOICE_LEAVE_PLANET:
				onPlanet = false;
				currPlanet = nullptr;
			}
		}
	}

	//Moves the player <distance> pixels towards their target
	void PlayerShip::MovePlayer(float distance)
	{
		Point newPos;

		newPos.x = distance * (float) cos(angleToTarget * PI / 180) + playerLocation.x;
		newPos.y = distance * (float) sin(angleToTarget * PI / 180) + playerLocation.y;

		playerLocation = newPos;
	}

	//Updates the player location based on the elapse time
	void PlayerShip::UpdateLocation(double dt)
	{
		//If the player is on a planet, don't move
		if (onPlanet)
		{
			isMoving = false;
			return;
		}

		//If the player is at their target, don't move
		if (playerLocation == targetLocation)
		{
			isMoving = false;
			return;
		}

		//Calc the vector to the player destination, and its length
		Vector v = targetLocation - playerLocation;
		float distance = v.Length();

		float pixelsToMove = (float) dt * PLAYER_SPEED;

		//If the distance to target is less than our travel distance, move there
		if (distance <= pixelsToMove)
		{
			playerLocation = targetLocation;
			isMoving = false;
		}

		//Calculate where to move the ship using trig
		else
		{
			MovePlayer(pixelsToMove);
			isMoving = true;
		}
	}

	//Checks if an enemy ship is within our hitbox
	int PlayerShip::CheckEnemyCollision(Point enemyLoc)
	{
		Vector v = playerLocation - enemyLoc;
		if (v.Length() < PLAYER_COLLISION_CIRCLE)
		{
			//If we have shields, break them
			if (hasShields)
			{
				hasShields = false;
				return 2;
			}

			//If we don't have shields, lose energy
			energy -= PLAYER_DEATH_ENERGY_LOSS;
			if (energy < 0)
			{
				energy = 0;
			}
			return 1;
		}

		return 0;
	}

	//Gets the science of the planet the player is on
	std::string PlayerShip::GetPScience()
	{
		if (currPlanet != nullptr)
		{
			return std::to_string(currPlanet->science);
		}
		return NULL;
	}

	//Gets the energy of the planet the player is on
	std::string PlayerShip::GetPEnergy()
	{
		if (currPlanet != nullptr)
		{
			return std::to_string(currPlanet->energy);
		}
		return NULL;
	}

	//checks if the player has collided with any planets
	int PlayerShip::CheckPlanetCollision(std::list<Planet*> planets)
	{
		for (auto p : planets)
		{
			if (p->isActive)
			{
				Vector v = playerLocation - p->planetLocation;

				//If the planet is within our hitbox, explore!
				if (v.Length() < PLAYER_COLLISION_CIRCLE)
				{
					targetLocation = playerLocation;
					p->isActive = false;
					onPlanet = true;
					currPlanet = p;
					return 1;
				}
			}
		}
		currPlanet = nullptr;
		return 0;
	}
}