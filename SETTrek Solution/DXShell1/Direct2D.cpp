/*
*  FILE          : Direct2D.cpp
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-May-29
*  DESCRIPTION   :
*    Initializes and handles some of the low-level D2D engine setup
*	 much of this is courtesy of https://bell0bytes.eu/game-programming/,
*	 but I made some adaptations to better fit SETTrek
*/

#pragma once

#include "Direct2D.h"
#include "SETTrek.h"
#include "Constants.h"

#include <math.h>

namespace graphics
{
	Direct2D::Direct2D(main::SETTrek* setTrek) : setTrek(setTrek)
	{
		//Initializes COM
		CoInitialize(NULL);

		CreateDevice();

		CreateBitmapRenderTarget();
		CreateDeviceIndependentResources();
		CreateDeviceDependentResources();
	}

	Direct2D::~Direct2D()
	{
		WICFactory.ReleaseAndGetAddressOf();

		CoUninitialize();
	}

	
	void Direct2D::CreateDevice()
	{
		DWriteCreateFactory(DWRITE_FACTORY_TYPE_SHARED, __uuidof(IDWriteFactory6), &writeFactory);
		CoCreateInstance(CLSID_WICImagingFactory2, NULL, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory2, &WICFactory);

		//Flag saying this won't be run in debug mode
		D2D1_FACTORY_OPTIONS options;
		options.debugLevel = D2D1_DEBUG_LEVEL_NONE;

		//Creates a multithreaded factory for creating D2D objects
		D2D1CreateFactory(D2D1_FACTORY_TYPE_MULTI_THREADED, __uuidof(ID2D1Factory7), &options, &factory);

		//Allows for interfacing between DXGI devices
		Microsoft::WRL::ComPtr<IDXGIDevice> dxgiDevice;
		setTrek->d3d->dev.Get()->QueryInterface(__uuidof(IDXGIDevice), &dxgiDevice);

		factory->CreateDevice(dxgiDevice.Get(), &dev);

		//Allows for rendering work across multiple threads. Increases performance in our UPER GRAPHICALLY HEAVY game :)
		dev->CreateDeviceContext(D2D1_DEVICE_CONTEXT_OPTIONS_ENABLE_MULTITHREADED_OPTIMIZATIONS, &devCon);

		return;
	}


	void Direct2D::CreateBitmapRenderTarget()
	{
		D2D1_BITMAP_PROPERTIES1 bp;
		//Uses the same colour format used for the D3D device, defined in Constants.h
		bp.pixelFormat.format = COLOUR_FORMAT;
		//Ignores the alpha value of all bitmaps
		bp.pixelFormat.alphaMode = D2D1_ALPHA_MODE_PREMULTIPLIED;
		//DPI of bitmaps. 96 is windows default (0 would also theoretically work, as it just uses the default in that case)
		bp.dpiX = 96.0f;
		bp.dpiY = 96.0f;
		//Render target can be used to ouptut to, but not as input
		bp.bitmapOptions = D2D1_BITMAP_OPTIONS_TARGET | D2D1_BITMAP_OPTIONS_CANNOT_DRAW;
		bp.colorContext = nullptr;

		//Get the DXGI version of the back buffer
		Microsoft::WRL::ComPtr<IDXGISurface> dxgiBuffer;
		setTrek->d3d->swapChain->GetBuffer(0, __uuidof(IDXGISurface), &dxgiBuffer);

		//Create the render target bitmap
		Microsoft::WRL::ComPtr<ID2D1Bitmap1> targetBitmap;
		devCon->CreateBitmapFromDxgiSurface(dxgiBuffer.Get(), &bp, &targetBitmap);

		//Sets the device context render target to the newly initialized render target
		devCon->SetTarget(targetBitmap.Get());

		//Sets the text AA to grayscale. Makes text less jagged, and tends to be a bit thicker than cleartext AA
		devCon->SetTextAntialiasMode(D2D1_TEXT_ANTIALIAS_MODE_GRAYSCALE);

		return;
	}


	void Direct2D::CreateDeviceIndependentResources()
	{
		InitializeTextFormats();
	}

	void Direct2D::CreateDeviceDependentResources()
	{
		CreateBrushes();
	}


	void Direct2D::CreateBrushes()
	{
		// create standard brushes
		devCon->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Yellow), &yellowBrush);
		devCon->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Black), &blackBrush);
		devCon->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::White), &whiteBrush);
		devCon->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Red), &redBrush);
		devCon->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Cyan), &blueBrush);
		devCon->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Brown), &brownBrush);
		devCon->CreateSolidColorBrush(D2D1::ColorF(D2D1::ColorF::Green), &greenBrush);
	}


	void Direct2D::InitializeTextFormats()
	{
		//Sets the font, weight, style (normal/italic/oblique), stretch, font size in DIP, locale (Canada baby!), and where to store it
		writeFactory->CreateTextFormat(L"Calibri", NULL, DWRITE_FONT_WEIGHT_HEAVY, DWRITE_FONT_STYLE_NORMAL, 
			DWRITE_FONT_STRETCH_NORMAL, 26.0f, L"en-CA", (IDWriteTextFormat **)textFormatPlanetStats.GetAddressOf());

		//Sets the allignment to the top left of the render target
		textFormatPlanetStats->SetTextAlignment(DWRITE_TEXT_ALIGNMENT_LEADING);
		textFormatPlanetStats->SetParagraphAlignment(DWRITE_PARAGRAPH_ALIGNMENT_NEAR);
	}

}