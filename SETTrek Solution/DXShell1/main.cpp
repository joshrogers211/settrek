/*
*  FILE          : main.cpp
*  PROJECT       : PROG 2215 - SET Trek III
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Feb-11
*  DESCRIPTION   :
*    A basic game that uses DirectX to draw a background, draw background sprites, and allow
*	 a player to control a ship and fly through space on an epic journey
*/

#include <Windows.h>
#include "SETTrekGame.h"

#pragma comment( lib, "winmm.lib" )


LRESULT CALLBACK WindowProc(
	HWND hwnd,
	UINT uMsg,
	WPARAM wParam,
	LPARAM lParam )
{
	if (uMsg == WM_DESTROY) { PostQuitMessage(0); return 0; }

	return DefWindowProc(hwnd, uMsg, wParam, lParam);
}


int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE prevInstance, LPWSTR cmd, int nCmdShow)
{
	//Create the game
	SETTrekGame game(hInstance);

	//Initialize and run the game
	game.InitGame();
	game.RunGame();

	game.EndScreen();

	game.Cleanup();

	return 0;
}
