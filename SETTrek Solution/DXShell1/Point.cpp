/*
*  FILE          : Point.cpp
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Mar-12
*  DESCRIPTION   :
*    Point and vector math for player movement and distance
*/

#pragma once

#include "Point.h"

namespace entities
{
	Vector::Vector(float newX, float newY) : x(newX), y(newY) {}

	//Calculates the length of a vector
	float Vector::Length() const
	{
		float l = (float) sqrt(x*x + y * y);

		return l;
	}

	//Vector scaling
	Vector Vector::operator*(float s) const
	{
		Vector v(x * s, y * s);
		return v;
	}

	//Vector scaling
	Vector Vector::operator/(float s) const
	{
		if (s != 0)
		{
			Vector v(x / s, y / s);
			return v;
		}
		return *this;
	}

	Point::Point(float newX, float newY) : x(newX), y(newY) {}


	//Adds a vector to a point
	Point Point::AddVector(Vector v)
	{
		Point newPos;

		newPos.x = x + v.x;
		newPos.y = y + v.y;

		return newPos;
	}

	//Check if two points are equal
	bool Point::operator==(Point a)
	{
		if ((a.x == x) && (a.y == y))
		{
			return true;
		}
		return false;
	}

	//Calculates a vector by subtracting two points
	Vector operator-(Point a, Point b)
	{
		Vector v;

		v.x = a.x - b.x;
		v.y = a.y - b.y;

		return v;
	}
}