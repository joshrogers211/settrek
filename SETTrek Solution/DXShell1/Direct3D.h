/*
*  FILE          : Direct3D.h
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Mar-29
*  DESCRIPTION   :
*    Handles some of the base setup for D3D.
*	 Adapted from https://bell0bytes.eu/game-programming/
*/

#pragma once

#include <wrl/client.h>

#include <d3d11_4.h>
#include <string>
#include <fstream>
#pragma comment (lib, "d3d11.lib")

class SETTrekGame;
namespace main
{
	class SETTrek;
	class Window;
}

namespace graphics
{
	struct VERTEX
	{
		float x, y, z;
		float r, g, b;
	};

	struct ConstantColourPositionBuffer
	{
		float x, y, z;						// position
		float spacing;						// spacing variable
		float r, g, b;						// colour
	};

	// shader buffer
	struct ShaderBuffer
	{
		BYTE* buffer;
		int size;
	};

	class Direct3D
	{
	private:
		main::SETTrek* setTrek;

		//Device object
		Microsoft::WRL::ComPtr<ID3D11Device> dev;
		//Device context
		Microsoft::WRL::ComPtr<ID3D11DeviceContext> devCon;
		//Swap chain, for frame buffering
		Microsoft::WRL::ComPtr<IDXGISwapChain> swapChain;
		//The main window view
		Microsoft::WRL::ComPtr<ID3D11RenderTargetView> renderTargetView;
		Microsoft::WRL::ComPtr<ID3D11DepthStencilView> depthStencilView;

		//Shaders, created with HLSL and compiled to CSO files
		Microsoft::WRL::ComPtr<ID3D11VertexShader> standardVertexShader;
		Microsoft::WRL::ComPtr<ID3D11PixelShader> standardPixelShader;

		Microsoft::WRL::ComPtr<ID3D11Buffer> constantColourPositionBuffer;

		// screen modes
		unsigned int numberOfSupportedModes;	// the number of supported screen modes for the desired colour format
		DXGI_MODE_DESC* supportedModes;			// list of all supported screen modes for the desired colour format
		DXGI_MODE_DESC  currentModeDescription;	// description of the currently active screen mode
		unsigned int currentModeIndex;			// the index of the current mode in the list of all supported screen modes
		bool startInFullscreen;					// true iff the game should start in fullscreen mode
		BOOL currentlyInFullscreen;				// true iff the game is currently in fullscreen mode
		bool changeMode;						// true iff the screen resolution should be changed this frame

		// functions to create resources
		DXGI_SWAP_CHAIN_DESC CreateSwapChainDesc(); //Fills a swap chain with the correct parameters
		void CreateResources();					//Create device resources, such as the swap chain
		void OnResize();						//Updates device params when window is resized

		ShaderBuffer LoadShader(std::wstring filename);	//Read shader data from .cso files

		void InitPipeline();					//Initialize the graphics rendering pipeline, apply shaders

		void ClearBuffers();					//Clears back buffers

		int Present();							//Flips frame buffers to present new frame
		

		Direct3D(main::SETTrek* setTrek);
		~Direct3D();

		friend class main::SETTrek;
		friend class Direct2D;
		friend class SETTrekGame;
		friend class main::Window;
	};
}