/*
*  FILE          : SETTrek.h
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Mar-26
*  DESCRIPTION   :
*    Base class for handling game timing, rendering, and game loops
*/

#pragma once

#include <Windows.h>
#include "GameTimer.h"
#include "Window.h"
#include "Direct2D.h"
#include "Direct3D.h"
#include "Constants.h"

namespace main
{
	class SETTrek
	{
	private:

		bool hasStarted;
		bool showFramerate;

		GameTimer* timer;
		const double dt;
		const double maxSkipFrames;

	protected:
		const HINSTANCE gameInstance;

		bool paused;
		bool run;

		float fps;
		double msPerFrame;

		const Window* gameWindow;

		graphics::Direct2D* d2d;
		graphics::Direct3D* d3d;

		SETTrek(HINSTANCE hInstance);
		~SETTrek();

		virtual void InitGame();
		virtual void Cleanup();

		virtual void OnKeyDown(WPARAM wParam, LPARAM lParam) = 0;
		virtual void OnClick(WPARAM wParam, int x, int y);

		virtual int RunGame();
		virtual int UpdateGameWorld(double deltaTime) = 0;

		void OnResize();

		virtual int Render(double farSeer) = 0;
		virtual void EndScreen() = 0;

		virtual void OnClickParent(int x, int y) = 0;

	public:
		friend class Window;
		friend class graphics::Direct2D;
		friend class graphics::Direct3D;
	};
}
