/*
*  FILE          : Graphics.h
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Feb-11
*  DESCRIPTION   :
*    Definitions for the graphics class, which handles game sprites and drawing
*/

#pragma once

#include <string.h>
#include <map>
#include <vector>
#include <wrl/client.h>
#include <d2d1_3.h>
#include <d2d1effects_2.h>

#include "Point.h"
#include "Constants.h"


namespace graphics
{
	class Direct2D;

	enum Chroma {NONE, GREEN, BLUE};

	class Sprite
	{
	protected:
		Direct2D* d2d;
		Microsoft::WRL::ComPtr<ID2D1Bitmap1> bmp;
		unsigned int drawOrder;
		float x, y;
		D2D1_SIZE_U pixelSize;
		D2D1_POINT_2F lastRotate;

		void Rotate(double degrees);
		void UnRotate();

	public:
		Sprite() {};
		Sprite(Direct2D *d2d, LPCWSTR fileName, float x, float y, Chroma chromaEffect = Chroma::NONE, bool scaleToSquare = false, unsigned int drawOrder = 0);
		~Sprite();

		entities::Point GetPosition();

		void DrawCircle(D2D1_ELLIPSE circle);
		static D2D1_SIZE_U LoadBMP(Direct2D* d2d, Microsoft::WRL::ComPtr<ID2D1Bitmap1>* src, LPCWSTR fileName);

		void Draw(double rotation = 0, D2D1_RECT_F* sourceRect = NULL, D2D1_RECT_F* destRect = NULL, bool flag = false);

		void SetSquare(int x, int y, D2D1_SIZE_U* drawableSize);
		void SetPosition(float newX, float newY);

		friend class SpriteMap;
		friend class AnimatedSprite;
	};

	struct AnimationInfo
	{
		LPCWSTR name;					// name of the animation
		unsigned int startFrame;		// the index of the first frame of an animation
		unsigned int numberOfFrames;	// the total numbers of frames in the animation
		float	width;					// the width of each frame
		float	height;					// the height of each frame
		float	rotationCenterX;		// rotation center x-coordinate
		float	rotationCenterY;		// rotation center y-coordinate
		float	paddingWidth;			// width of the padding
		float	paddingHeight;			// height of the padding
		float	borderPaddingWidth;	// width of the border padding
		float	borderPaddingHeight;	// height of the border padding
	};

	class AnimatedSpriteData
	{
	private:
		Microsoft::WRL::ComPtr<ID2D1Bitmap1> animatedSpriteSheet;
		std::vector<AnimationInfo> animationInfo;

	public:
		AnimatedSpriteData(Direct2D* d2d, LPCWSTR spriteSheetFile, std::vector<AnimationInfo> frameData, Chroma chromaEffect = Chroma::NONE);
		~AnimatedSpriteData();

		friend class AnimatedSprite;
	};


	class AnimatedSprite : public Sprite
	{
	private:
		AnimatedSpriteData* animSpriteData;
		unsigned short currFrame;
		float animFPS = ANIMATION_FPS;
		double frameAge;

	public:
		AnimatedSprite(Direct2D* d2d, AnimatedSpriteData* animatedSpriteData, float animationFPS = ANIMATION_FPS,
			float x = 0.0f, float y = 0.0f, unsigned int drawOrd = 0);

		~AnimatedSprite();

		void Draw();

		void UpdateTime(double deltaTime);
	};
}