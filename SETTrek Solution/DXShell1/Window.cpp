/*
*  FILE          : Windows.cpp
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Mar-12
*  DESCRIPTION   :
*    Handles base window creation and message handling
*/

#include "window.h"
#include "SETTrek.h"
#include <windowsx.h>

#include "Constants.h"

namespace
{
	main::Window* window = NULL;
}

namespace main
{
	LRESULT CALLBACK mainWndProc(HWND hWnd, unsigned int msg, WPARAM wParam, LPARAM lParam)
	{
		return window->msgProc(hWnd, msg, wParam, lParam);
	}

	//Gets window params
	Window::Window(SETTrek* setTrek) : gameWindow(NULL), setTrek(setTrek)
	{
		window = this;

		gameWidth = DEFAULT_WINDOW_WIDTH;
		gameHeight = DEFAULT_WINDOW_HEIGHT;
		gameWindow = NULL;

		this->initializeWindow();
	}

	Window::~Window()
	{
		if (gameWindow)
		{
			gameWindow = NULL;
		}

		if (setTrek)
		{
			setTrek = NULL;
		}
	}

	//Initializes the game window
	int Window::initializeWindow()
	{
		//Do a bit of reading - What is this Window Class used for? 
		// What are the major parameters below?
		WNDCLASSEX windowclass;
		ZeroMemory(&windowclass, sizeof(WNDCLASSEX));    //Clears out the possible junk in memory for the windowclass
		windowclass.cbSize = sizeof(WNDCLASSEX);    //Size of the structure. Always sizeof(WNDCLASSEX)
		windowclass.cbWndExtra = 0;
		windowclass.hbrBackground = (HBRUSH)COLOR_WINDOW;    //Sets the window background colour to white
		windowclass.hInstance = setTrek->gameInstance;    //Handle used for registering and creating the window
		windowclass.lpfnWndProc = mainWndProc;    //Sets the function called on received messages
		windowclass.lpszClassName = "MainWindow";    //Sets the title of the window
		windowclass.style = CS_HREDRAW | CS_VREDRAW;    //Redraws the entire window if it's resized Vertically or Horizontally

		RegisterClassEx(&windowclass);

		RECT rect = { 0, 0, (long)gameWidth, (long)gameHeight };    //Defines the top left and bottom right corners of a rectangle
		if (!AdjustWindowRectEx(&rect, WS_OVERLAPPED, false, WS_EX_OVERLAPPEDWINDOW))    //Sets the window's size based on the rectangle defined earlier
		{
			return -1;
		}

		gameWindow = CreateWindowEx(WS_EX_OVERLAPPEDWINDOW, "MainWindow", WINDOW_TITLE, WS_OVERLAPPEDWINDOW, 100, 100,
			rect.right - rect.left, rect.bottom - rect.top, NULL, NULL, setTrek->gameInstance, 0);
		if (!gameWindow)
		{
			return -1;
		}

		ShowWindow(gameWindow, SW_SHOW);
		UpdateWindow(gameWindow);

		return 0;
	}

	//Hacky method to exiting the game
	void Window::SendUserMessage(unsigned int msg, int param) const
	{
		WPARAM wParam = param;
		LPARAM lParam = param;
		
		SendMessage(gameWindow, msg, param, param);
	}

	//Main message loop
	LRESULT CALLBACK Window::msgProc(HWND hWnd, unsigned int msg, WPARAM wParam, LPARAM lParam)
	{
		switch (msg)
		{
		case WM_ACTIVATE:
			if (LOWORD(wParam) == WA_INACTIVE)
			{
				setTrek->paused = true;
				setTrek->timer->Stop();
			}
			else {
				if (setTrek->hasStarted)
				{
					setTrek->timer->Start();
				}
				setTrek->paused = false;
			}
			return 0;

		case WM_DESTROY:
			PostQuitMessage(0);

		case WM_CLOSE:
			setTrek->paused = true;
			setTrek->timer->Stop();

			if (MessageBoxExW(gameWindow, L"Are you sure you want to quit? The sector is still in danger!", L"Abandon the sector?",
				MB_YESNO | MB_ICONQUESTION, 0) == IDYES)
			{
				return DefWindowProc(gameWindow, msg, wParam, lParam);
			}
			else
			{
				setTrek->paused = false;
				setTrek->timer->Start();
				return 0;
			}
		case WM_USER:
			setTrek->EndScreen();

		case WM_KEYDOWN:
			setTrek->OnKeyDown(wParam, lParam);
			return 0;
		case WM_RBUTTONDOWN:
		case WM_LBUTTONDOWN:
			setTrek->OnClick(wParam, GET_X_LPARAM(lParam), GET_Y_LPARAM(lParam));
		}

		return DefWindowProc(hWnd, msg, wParam, lParam);
	}
}