/*
*  FILE          : Missile.h
*  PROJECT       : PROG 2215 - SET Trek
*  PROGRAMMER    : Josh Rogers 8230351
*  FIRST VERSION : 2019-Apr-11
*  DESCRIPTION   :
*    FIRE THE MISSILES! Handles the movement and drawing of player-fired missiles
*/

#pragma once
#include "Graphics.h"
#include "Point.h"
#include "PlayerShip.h"
#include "EnemyShip.h"

namespace entities
{
	class Missile
	{
		graphics::Sprite* missileSprite;
		EnemyShip* enemyShip;
		Point missileLocation;
		double angleToTarget;
		bool isActive;
		
	public:
		Missile(graphics::Sprite* sprite) : missileSprite(sprite) {}
		~Missile() { delete missileSprite; }

		//Flags the missile as enabled, starts the missile at the player
		void FireMissile(PlayerShip* pShip, EnemyShip* eShip)
		{
			isActive = true;
			missileLocation = pShip->GetLocation();
			pShip->MissileFired();
			enemyShip = eShip;
			CalcTargetAngle();
		}

		//Calculates the angle between the missile and the enemy
		void CalcTargetAngle()
		{
			if (!isActive)
			{
				angleToTarget = 0;
				return;
			}
			Vector v = missileLocation - enemyShip->GetLocation();
			angleToTarget = atan2(v.y, v.x) * 180 / PI + 180;
		}

		//Moves the missile the desired number of pixels towards the enemy ship
		void MoveMissile(float pixelsToMove)
		{
			Point newPos;

			newPos.x = pixelsToMove * (float)cos(angleToTarget * PI / 180) + missileLocation.x;
			newPos.y = pixelsToMove * (float)sin(angleToTarget * PI / 180) + missileLocation.y;

			//Recalculates the target angle, since both entities are moving
			CalcTargetAngle();

			missileLocation = newPos;
		}

		//Updates the missile's location based on the elapsed time
		void UpdateLocation(double dt)
		{
			//If the missile is active and the enemy ship exists
			if (isActive && enemyShip != nullptr)
			{
				//Calc vector between missile and enemy ship
				Vector v = enemyShip->GetLocation() - missileLocation;
				float distance = v.Length();

				//Calc how far to move based on speed and time since last move
				float pixelsToMove = (float)dt * MISSILE_SPEED;

				//If we're on top of the enemy, the enemy takes damage and the missile disappears
				if (distance <= pixelsToMove)
				{
					enemyShip->TakeDamage();
					isActive = false;
				}
				//Else, move towards the enemy
				else 
				{
					MoveMissile(pixelsToMove);
				}
			}
		}

		//Draws the missile if it's active
		void Draw()
		{
			if (isActive)
			{
				missileSprite->SetPosition(missileLocation.x, missileLocation.y);
				missileSprite->Draw(angleToTarget, NULL, NULL, true);
			}
		}
	};
}